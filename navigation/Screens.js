import React from 'react';
import { Easing, Animated, Dimensions,TouchableOpacity } from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { Block, Text, theme } from "galio-framework";
//import {Icon} from 'react-native-elements';
import HomeScreen from '../screens/Home';
// import ResultsScreen from '../screens/ResultsScreen';
// import LeaderboardScreen from '../screens/LeaderboardScreen';
import AddMatchResultsScreen from '../screens/AddMatchResultScreen';
import SelectWinnerScreen from '../screens/SelectWinnerScreen';
import OnboardingScreen from '../screens/Onboarding';
import ProfileScreen from '../screens/Profile';
import EditProfileScreen from '../screens/EditProfile';
import UploadImageScreen from '../screens/UploadImage';
import UploadCourtImagesScreen from '../screens/UploadCourtImages';
import EditTimeSlotsScreen from '../screens/EditTimeslots';
import CalendarScreen from "../screens/Calendar";
import CalendarSelecterScreen from '../screens/CalendarSelecter';
// New login and signup routes
import LoginScreen from '../screens/LoginCourt';
import SignupScreen from '../screens/SignupCourt';

//Reservations page
import ReservationScreen from '../screens/Reservation';

import { Icon } from '../components';
import { Images, materialTheme } from "../constants/";

const { width } = Dimensions.get("screen");

const Stack = createStackNavigator();
const TopTab = createMaterialTopTabNavigator();
const Tab = createBottomTabNavigator();

import firebase from '../firebase'


function signOut(props) {
  const {navigation} = props;
  firebase.auth()
  .signOut()
  .then(() => {
    console.log('User signed out!');
    navigation.navigate("Login");
  });
}

// Login


function MatchesResultsStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen" 
    options={{
      headerTitleAlign:"center"
    }}>
       {/* <Stack.Screen 
        name="matchResults"
        component={MatchResultsTab}
        options={{
          headerTitle:"Match Results",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
        
      /> */}
       <Stack.Screen
        name="AddMatchResults"
        component={AddMatchResultsScreen}
        options={{
          headerTitle:"Add new Result",
          headerTitleAlign:"center",
          headerLeft:null,
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
      <Stack.Screen
        name="SelectWinner"
        component={SelectWinnerScreen}
        options={{
          headerTitle:"Select the Winner",
          headerTitleAlign:"center",
          headerLeft:null,
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
      {/* <Stack.Screen
        name="New Match"
        component={ResultsScreen}
        options={{
          headerTitle:"results",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
      <Stack.Screen
        name="leaderboard"
        component={LeaderboardScreen}
        options={{
          headerTitle:"Select team",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      /> */}
    </Stack.Navigator>
  );
}

function HomeStack(props) {
  return (
    <Stack.Navigator 
    //mode="modal" 
   // headerMode="screen"
    >
      <Stack.Screen 
        name="Home"
        component={HomeScreen}
        options={{
          headerTitle:"Home",
          headerTitleAlign:"center",
          headerLeft:null,
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
     
      {/* <Stack.Screen
                name="Result"
                component={MatchesResultsStack}
                /> */}
       {/* <Stack.Screen 
        name="matchResults"
        component={MatchResultsTab}
        
      /> */}
    </Stack.Navigator>
  );
}

function CalendarStack(props){
  return(
    <Stack.Navigator initialRouteName="Calendar">
      <Stack.Screen 
        name="Calendar"
        component={CalendarScreen}
        options={{
          headerTitle:"Calendar",
          headerTitleAlign:"center",
          headerLeft:null,
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
      <Stack.Screen 
        name="CalendarSelecter"
        component={CalendarSelecterScreen}
        options={{
          headerTitle:"Calendar",
          headerTitleAlign:"center",
          headerLeft:null,
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
    </Stack.Navigator>
  )
}
function ProfileStack(props){
 // const {navigation} = props;
  return(
  <Stack.Navigator mode="card" headerMode="screen">
    <Stack.Screen
          name="Profile"
          component={ProfileScreen}
          options={({navigation})=>({
          headerTitle:"Profile",
          headerTitleAlign:"center",
          headerLeft:null,
          headerRight:()=>(
            <TouchableOpacity onPress={()=>signOut(props)}>
             <Icon
                  name='ios-log-out'
                  family='ionicon'
                  color='black'
                  size={26}
                  style={{paddingRight: 10,}}
                />
            </TouchableOpacity>),
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
          })}
          />
       <Stack.Screen
          name="EditProfile"
          component={EditProfileScreen}
          options={{
          headerTitle:"Edit Profile",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
          }}
          />
          <Stack.Screen
          name="UploadImage"
          component={UploadImageScreen}
          options={{
          headerTitle:"Upload Image",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
          }}
          />
          <Stack.Screen
          name="UploadCourtImages"
          component={UploadCourtImagesScreen}
          options={{
          headerTitle:"Upload Court Images",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
          }}
          />
          <Stack.Screen
          name="EditTimeSlots"
          component={EditTimeSlotsScreen}
          options={{
          headerTitle:"Edit TimeSlots",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
          }}
          />
  </Stack.Navigator>
  )
}


// function MatchResultsTab(props) {
//   return (
//     <TopTab.Navigator mode="card" headerMode="screen" tabBarOptions={{pressColor:"#3BAD36",indicatorStyle:{backgroundColor:"#3BAD36"}}}>
//       <TopTab.Screen name="results" component={ResultsScreen}/>
//       <TopTab.Screen name="leaderboard" component={LeaderboardScreen}  />
//     </TopTab.Navigator>
//   );
// }

// function AppStack(props) {
//   return (
//     <Stack.Navigator mode="card" headerMode="none">
//       <Stack.Screen
//                 name="Home"
//                 component={HomeStack}
//                 />
//       <Stack.Screen
//                 name="Result"
//                 component={MatchesResultsStack}
//                 />

function ReservationStack(props) {
  return (
    <Stack.Navigator 
    //mode="modal" 
   // headerMode="screen"
    >
      <Stack.Screen 
        name="Reservation"
        component={ReservationScreen}
        options={{
          // headerTitle:"Reservations",
          // headerTitleAlign:"center",
          // headerLeft:null,
          // headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
          headerShown: false
        }}
      />
    </Stack.Navigator>
  );
}

function AppStack(props) {
  
  return (
    <Tab.Navigator
    initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#3BAD36',
       // inactiveTintColor:'#000'
      }}
      
      >
      <Tab.Screen name="Home" component={HomeStack}
        options={{tabBarLabel: 'Home',
        tabBarIcon: ({focused}) => <Icon name='ios-home'
                                family='ionicon'
                                size={26} 
                                color={focused ? "#3BAD36" : "#000"}/>,
        
        
      }}
        />
        <Tab.Screen name="Calendar" component={CalendarStack}
        options={{tabBarLabel: 'Calendar',
        tabBarIcon: ({focused}) => <Icon name='ios-calendar'
                                family='ionicon'
                                size={26} 
                                color={focused ? "#3BAD36" : "#000"}/>,
        
        
      }}
        />

      <Tab.Screen name="Reservations" component={ReservationStack}
       options={{tabBarLabel: 'Reservations',
       tabBarIcon: ({focused}) => <Icon name='calendar-check-o'
                               family='font-awesome'
                               size={26} 
                               color={focused ? "#3BAD36" : "#000"}/>,

        }}
      />
    <Tab.Screen name="MatchResults" component={MatchesResultsStack}
       options={{tabBarLabel: 'Results',
       tabBarIcon: ({focused}) => <Icon name='trophy'
                               family='font-awesome'
                               size={26} 
                               color={focused ? "#3BAD36" : "#000"}/>,

        }}
      />
      <Tab.Screen name="Profile" component={ProfileStack}
       options={{tabBarLabel: 'Profile',
       tabBarIcon: ({focused}) => <Icon name='user'
                               family='font-awesome'
                               size={26} 
                               color={focused ? "#3BAD36" : "#000"}/>,

     }}
      />
    </Tab.Navigator>
  );
}

export default function OnboardingStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="none" initialRouteName={firebase.auth().currentUser ? 'App' : 'Login'}>
       <Stack.Screen
        name="Loading"
        component={OnboardingScreen}
        option={{
          headerTransparent: true
        }}
      /> 
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        option={{
          headerTransparent: true
        }}
      />
      <Stack.Screen
        name="Signup"
        component={SignupScreen}
        option={{
          headerTransparent: true
        }}
      />
      <Stack.Screen name="App" component={AppStack} 
      options={{
        headerLeft:null
      }}
      />
    </Stack.Navigator>
  );
}


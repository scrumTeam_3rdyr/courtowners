import Match from '../models/match';
import Leaderboard from '../models/leaderboard';

 export const matches = [
    new Match('home team1', 'Team1', 'https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','Aug 25th 20','Friday at 4:57 PM'),
    new Match('Team2', 'Team22', 'https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','Aug 25th 20','Friday at 4:57 PM'),
    new Match('Team3', 'Team33', 'https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','Aug 25th 20','Wednesday at 4:57 PM'),
    new Match('Team4', 'Team44', 'https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','Aug 25th 20','Friday at 4:57 PM'),
    new Match('Team5', 'Team55', 'https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','Aug 25th 20','Friday at 4:57 PM'),
    new Match('Team6', 'Team66', 'https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','Aug 25th 20','Friday at 4:57 PM'),
    new Match('Team7', 'Team77', 'https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','Aug 25th 20','Friday at 4:57 PM'),
    new Match('Team8', 'Team88', 'https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','Aug 25th 20','Friday at 4:57 PM'),
    new Match('Team9', 'Team99', 'https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','https://image.freepik.com/free-vector/e-sports-team-logo-template-with-king_23-2147821565.jpg','Aug 25th 20','Friday at 4:57 PM')

 ];

 export const scores = [
    new Leaderboard('Team Fighters ', 222,1,3,+2),
    new Leaderboard('Pendragons', 2,1,3,+2),
    new Leaderboard('Flash', 2,0,1,+4),
    new Leaderboard('Team Red', 2,1,3,+2),
    new Leaderboard('Team 5', 2,1,3,+2),
    new Leaderboard('Team 6', 2,1,3,+2),
 ]


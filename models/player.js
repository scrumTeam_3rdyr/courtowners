class Player{
    constructor(email,username,mobile,current_team,gender,status,wins,loss){
        this.email = email,
        this.username=username,
        this.mobile = mobile,
        this.current_team = current_team,
        this.gender = gender,
        this.status = status,
        this.wins = wins,
        this.loss = loss   
    }
}
export default Player;
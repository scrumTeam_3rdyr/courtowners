// teamName
// wins
// losses
// playedMatches
// Points
class Leaderboard{
    constructor(teamId,teamName,wins,losses,matchCount,points){
        this.teamId = teamId;
        this.teamName = teamName;
        this.wins = wins;
        this.losses = losses;
        this.matchCount = matchCount;
        this.points = points;
    }
}
export default Leaderboard;
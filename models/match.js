// class Match{
//     constructor(team1,team2,date,time,winningTeam){
//         this.team1 = team1,
//         this.team2 = team2,
//         this.date = date,
//         this.time = time,
//         this.winningTeam = winningTeam
//     }
// }
// export default Match;

class Match{
    constructor(id,team1Id,team1Name,team1ImageUrl,team2Id,team2Name,team2ImageUrl,date,time,isResultsUpdated,winner){
        this.id = id;
        this.team1Id = team1Id;
        this.team1Name = team1Name;
        this.team1ImageUrl = team1ImageUrl;
        this.team2Id = team2Id;
        this.team2Name = team2Name;
        this.team2ImageUrl = team2ImageUrl;
        this.date = date;
        this.time = time;
        this.isResultsUpdated = isResultsUpdated;
        this.winner = winner;
    }
}
export default Match;
import React,{useState,useEffect} from 'react'
import{
    StyleSheet,Button,
    Dimensions, ScrollView,View, Alert,Switch
} from 'react-native';
import{
    Block,
    Text,
    theme
} from 'galio-framework';
const { width } = Dimensions.get('screen');
import CalendarPicker from 'react-native-calendar-picker'
import moment from 'moment'
import firebase from '../firebase'

function Calendar(props){

    const [StartDate, setStartDate] = useState();
    const [holiday,setHoliday]=useState(false);
    const [matchList, setMatchList]=useState([])
    const {navigation} = props;
    const courtRef=firebase.firestore().collection('court').doc(firebase.auth().currentUser.email).collection('availabletimeslots');
    
    useEffect(() => {
      populateDays().then((result)=>{
        let holidayList= result.map(function(val,index){
          return val.data();
        })
        console.log(holidayList);
        setMatchList(holidayList)
      })
    }, [])

    const populateDays = async () =>{
      console.log("test days");
      let availableDays= await courtRef.get();
      return availableDays.docs;
    }
    useEffect(() => {
      const dateObj = moment(startingDate).format('MMM Do YY')
        const filter = matchList.filter((item)=>{
          if(item.date==dateObj){
            if(item.holiday==true){
              console.log(item)
            setHoliday(true);
            return true;
          }
          }
          return false;
          
        })
    }, [StartDate]);
    const onDateChange=(date) =>{
        setHoliday(false);
        setStartDate(date);     
      }
      
    const updateStatus =async () =>{
      const dateObj1 = moment(startingDate).format('DDMMMYYYY');
      const dateObj2 = moment(startingDate).format('MMM Do YY');
      console.log(dateObj1 + dateObj2);
      const setFirebase= await courtRef.doc(dateObj1).set({date:dateObj2, holiday:!holiday});
      setHoliday(!holiday)
    }
    const changeHoliday = () =>{
      console.log(holiday)
      Alert.alert(
        "Alert",
        "Do you wish to change holiday?",
        [
          {text:"YES", onPress:()=>updateStatus()},
          {text:"NO", onPress:()=>setHoliday(holiday)}
        ]
      )
      
    }
    const startingDate = StartDate ? StartDate.toString() : '';
    const dateObj = moment(startingDate).format('MMM Do YY')
    //   const dateonly = dateObj.toISOString();
    return (
        <Block flex center style={styles.home}>
            <View>
                <CalendarPicker
                    onDateChange={onDateChange}
                    startFromMonday
                />
                <View>
                {/* <Text>SELECTED DATE:{ dateObj }</Text> */}
                <Button onPress={()=>navigation.navigate("CalendarSelecter",{dateValue:StartDate})} title="Select Date"/>
              </View>
              <View style={{paddingTop:10,paddingLeft:10, flexDirection:'row'}}>
                <Text>Toggle Holiday :</Text>
              <Switch
                  value={holiday}
                  onValueChange={() => {changeHoliday()} }
                  style={{alignSelf:"flex-start"}}
                />
              </View>
            </View>            
        </Block>
      );
}
export default Calendar;

const styles = StyleSheet.create({
    home: {
      width: width,    
    },
    search: {
      height: 48,
      width: width - 32,
      marginHorizontal: 16,
      borderWidth: 1,
      borderRadius: 3,
    },
    header: {
      backgroundColor: theme.COLORS.WHITE,
      shadowColor: theme.COLORS.BLACK,
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowRadius: 8,
      shadowOpacity: 0.2,
      elevation: 4,
      zIndex: 2,
    },
    tabs: {
      marginBottom: 24,
      marginTop: 10,
      elevation: 4,
    },
    tab: {
      backgroundColor: theme.COLORS.TRANSPARENT,
      width: width * 0.50,
      borderRadius: 0,
      borderWidth: 0,
      height: 24,
      elevation: 0,
    },
    tabTitle: {
      lineHeight: 19,
      fontWeight: '300'
    },
    divider: {
      borderRightWidth: 0.3,
      borderRightColor: theme.COLORS.MUTED,
    },
    products: {
      width: width - theme.SIZES.BASE * 2,
      paddingVertical: theme.SIZES.BASE * 2,
    },
  });
  
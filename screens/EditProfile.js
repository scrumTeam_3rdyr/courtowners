import React,{useState,useEffect} from 'react'
import {View, StyleSheet, Button, Alert} from 'react-native'
import {Avatar} from 'react-native-elements'
import { Block,Text, Input } from 'galio-framework';
import ImagePicker from 'react-native-image-picker';

import { materialTheme } from '../constants';
import firebase from '../firebase'


function EditProfile(props){
    const {navigation} = props
    const courtDB=firebase.firestore().collection('court');
    const [Address, setAddress] = useState('');
    const [Name, setName] = useState('');
    const [ContactNo, setContactNo] = useState('');
    const [ProfilePicture, setProfilePicture] = useState(null);
    const user = firebase.auth().currentUser.email;

    useEffect(()=>{
      var query=courtDB.doc(user).get().then(
        (doc)=>{
            let data = doc.data();
            setAddress(data.address)
            setName(data.court_name)
            if(data.contact_no){setContactNo(data.contact_no)}
            if(data.imageuri){setProfilePicture(data.imageuri);}
          }
      );
    },[])
    
    const updateCourt=()=>{
        Alert.alert(
            "Alert",
            "Do you wish to update the details?",
            [
                {text:"YES", onPress:()=>{
                    courtDB.doc(user).update({court_name:Name,address:Address,contact_no:ContactNo})
                }, style:"cancel"},
                {text:"NO", onPress:()=>console.log("NO"), style:"cancel"}
            ]
        )
    }
    return(
        <View style={styles.container}>
            <Avatar
                rounded
                size="xlarge"
                source={{ uri:ProfilePicture}}
                onPress={()=>navigation.navigate("UploadImage")}
                showAccessory={true}
            />
            <Input placeholder="Name" 
            rounded value={Name} style={styles.input}
            color='black'
            onChangeText={(val)=>setName(val)}/>
            <Input placeholder="Address"rounded value={Address} color='black'
            onChangeText={(val)=>setAddress(val)}/>
            <Input placeholder="Contact Number" rounded value={ContactNo} color='black'
            onChangeText={(val)=>setContactNo(val)}/>
            <Block style={styles.button}>
                <Button title="Update" color={materialTheme.COLORS.GREEN} onPress={()=>updateCourt()}/>
            </Block>
        </View>
    )
}
const styles=StyleSheet.create({
    container:{
        alignItems:'center',
        padding:20
    },
    button:{
        alignSelf:'flex-end',
        paddingTop:20,
    },
    input:{
        color:'black'
    }
    
})
export default EditProfile;
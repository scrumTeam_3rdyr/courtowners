import React from 'react';
import { ImageBackground, StyleSheet, StatusBar, Dimensions, Platform } from 'react-native';
import { Block, Button, Text, theme } from 'galio-framework';

const { height, width } = Dimensions.get('screen');
import firebase from '../firebase'

export default class Onboarding extends React.Component {

    componentDidMount(){
        if(firebase.auth().currentUser){
            this.props.navigation.navigate("App")
        }
        else{
            this.props.navigation.navigate("Login")
        }
    }   
    componentDidUpdate(){
        if(firebase.auth().currentUser){
            this.props.navigation.navigate("App")
        }
        else{
            this.props.navigation.navigate("Login")
        }
    } 

  render() {
    const { navigation } = this.props;

    return (
      <Block flex style={styles.container}>
        <StatusBar barStyle="light-content" />
        <Block flex center>
        </Block>
        <Block flex space="between" style={styles.padded}>
          <Block flex space="around" style={{ zIndex: 2 }}>
            <Block>
              <Block>
                <Text color="black" size={60}>Sports Zone</Text>
              </Block>
              <Text size={16} color='black'>
                Welcome Player!
              </Text>
            </Block>

          </Block>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.COLORS.WHITE,
  },
  padded: {
    paddingHorizontal: theme.SIZES.BASE * 2,
    position: 'relative',
    bottom: theme.SIZES.BASE,
  },
  button: {
    width: width - theme.SIZES.BASE * 4,
    height: theme.SIZES.BASE * 3,
    shadowRadius: 0,
    shadowOpacity: 0,
  },
});

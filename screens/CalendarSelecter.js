import React,{useState,useEffect} from 'react'
import{
    StyleSheet,FlatList,Image,TouchableOpacity,
    Dimensions, ScrollView,View,RefreshControl
} from 'react-native';
import{
    Block,
    Text,
    theme
} from 'galio-framework';
import { Icon, Product } from '../components/';
const { width } = Dimensions.get('screen');
import CalenderStrip from 'react-native-calendar-strip'
import moment from 'moment'
import { materialTheme } from '../constants';
import firebase from '../firebase';
import Theme from '../constants/Theme'

const wait = (timeout) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}

function CalendarSelecter({navigation, route}){
    const {dateValue}=route.params;
    console.log(dateValue);
    const [StartDate, setStartDate] = useState("");
    const user = firebase.auth().currentUser.email;
    const courtRef = firebase.firestore().collection('court');
    const [courtDetails,setCourtDetails]=useState();
    const matches= firebase.firestore().collection('matches');
    const [matchList,setmatchList]=useState([]);
    const [matchesDay,setMatchesDay]=useState([]);
    const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, []);
    // useEffect(() => {
    //   let court=courtRef.doc(user).get().then(
    //     (doc)=>{
    //         setCourtDetails(doc.data());
    //         console.log(courtDetails);
    //       }
    //   );
    // }, [])
    useEffect(() => {
      if(user){
      findmatches().then((result)=>{
        let matchlist=result.map(function(val,index){
          return val.data();
        })
        console.log(matchlist);
        setmatchList(matchlist);
      })
    }
    }, [refreshing])
    // useEffect(()=>{
    //     let dayList=matchList.filter((item)=>{
    //      if(item.date===StartDate && item.matchStatus!="pending"){
    //       return true
    //     }
    //     else{
    //       return false
    //     }
    //     })
    //     console.log("list",dayList);
    //     setMatchesDay(dayList);
    // },[StartDate])
///.where('courtName','==',courtDetails.court_name)
  const findmatches=async ()=>{
    // console.log(courtDetails.court_name);
    const querySnapshot = await matches.where('court_email','==',user).where("matchStatus","==","accepted").get();
    //console.log(querySnapshot.docs);
    return querySnapshot.docs;
}
    const onDateChange=(date) =>{
      const dateObj = moment(date).format('MMM Do YY');
      const stringDate = dateObj.toString();
        setStartDate(stringDate);
        let dayList=matchList.filter((item)=>{
          if(item.date===StartDate && item.matchStatus!="pending"){
           return true
         }
         else{
           return false
         }
         })
         console.log("list",dayList);
         setMatchesDay(dayList);
      }
    const startingDate = StartDate ? StartDate.toString() : '';

    //   const dateonly = dateObj.toISOString();

    const renderMatches=()=>{
        return (

          <FlatList
               data={matchesDay}
               renderItem={({item})=>
               <View style={styles.list}>

              {/* <Image source = {{uri:item.opponentTeamLogoUrl}} style={styles.imageView} /> */}
              <View style={{flex:2, flexDirection: 'column',justifyContent:"center", paddingLeft:10}}>
                  <Text size={16} style={{alignSelf: 'flex-start',}}>{item.opponentTeamName}</Text>
                  <Text size={16}>VS</Text>
                  <Text size={16}>{item.teamName}</Text>
              </View>
              {/* <View style={{flex:2, flexDirection: 'column',justifyContent:"center"}}>
                  <Text size={16}>{item.teamName}</Text>
              </View> */}
              {/* <Image source = {{uri:item.teamLogoUrl}} style={styles.imageView} /> */}
               <View style={{flex:4, flexDirection: 'column', alignItems:"flex-end",justifyContent:"center", paddingRight:5}}>
               <Text>{item.time}</Text>
               {/* <Text >{item.date}</Text> */}
               </View>
               <View style={{flex:2, flexDirection: 'column', justifyContent:"center" }}>
               </View>
             </View>
             }
             keyExtractor={item => item.requestId}
             />
     )
    }
    return (
        <Block flex center>
            <ScrollView refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>
                <CalenderStrip
                  scrollable
                  startingDate={dateValue}
                  style={{height:100, paddingTop: 20, paddingBottom: 10,width:width}}
                  selectedDate={dateValue}
                  onDateSelected={onDateChange}
                  calendarColor={materialTheme.COLORS.GREEN}
                  calendarHeaderStyle={{color: 'white'}}
                  dateNumberStyle={{color: 'white'}}
                  dateNameStyle={{color: 'white'}}
                  iconContainer={{flex: 0.1}}
                />

                <View>
                {/* <Text>SELECTED DATE:{ StartDate }</Text> */}
                </View>
                <ScrollView>
                  {renderMatches()}
                </ScrollView>
                </ScrollView>
                </Block>
      );
}
export default CalendarSelecter;

const styles = StyleSheet.create({

    search: {
      height: 48,
      width: width - 32,
      marginHorizontal: 16,
      borderWidth: 1,
      borderRadius: 3,
    },
    header: {
      backgroundColor: theme.COLORS.WHITE,
      shadowColor: theme.COLORS.BLACK,
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowRadius: 8,
      shadowOpacity: 0.2,
      elevation: 4,
      zIndex: 2,
    },
    tabs: {
      marginBottom: 24,
      marginTop: 10,
      elevation: 4,
    },
    tab: {
      backgroundColor: theme.COLORS.TRANSPARENT,
      width: width * 0.50,
      borderRadius: 0,
      borderWidth: 0,
      height: 24,
      elevation: 0,
    },
    tabTitle: {
      lineHeight: 19,
      fontWeight: '300'
    },
    divider: {
      borderRightWidth: 0.3,
      borderRightColor: theme.COLORS.MUTED,
    },
    products: {
      width: width - theme.SIZES.BASE * 2,
      paddingVertical: theme.SIZES.BASE * 2,
    },
    imageView: {
      width: width/5,
      height: width/5 ,
      margin: 7,
      borderRadius : 7,
  },
  list:{
    flex:1,
    flexDirection: 'row',
    justifyContent:"space-evenly",
    backgroundColor:"#fff",
    //borderRadius:25,
    borderBottomWidth:10,
    borderBottomColor:Theme.COLORS.BORDER_COLOR,
    paddingVertical:10,
  },
  });

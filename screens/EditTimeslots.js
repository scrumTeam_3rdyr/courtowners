import { Block, Text } from 'galio-framework';
import React, { useEffect, useState } from 'react'
import { ScrollView, Button,RefreshControl } from 'react-native';
import timeslotmodel from '../models/timeslotModel'

import firebase from '../firebase'
import CheckBox from '@react-native-community/checkbox'

const wait = (timeout) => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  }

function EditTimeSlots({navigation}){
    const [timeSlotList,setTimeSlotList]=useState([]);
    const courtDB=firebase.firestore().collection('court');
    const user = firebase.auth().currentUser.email;
    const [timeSlotModelList,setTimeSlotModelList]=useState([]);
    const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, []);

    useEffect(()=>{
        var query=courtDB.doc(user).get().then(
            (doc)=>{
                let data = doc.data();
                if(data.timeSlot){setTimeSlotList(data.timeSlot)}
    }
        )
    },[])
    useEffect(() => {
        let result=timeslotmodel.map(function(val,index) {
            if(timeSlotList.includes(val)){
                return {time:val, selected:true}
            }
            else{
                return {time:val, selected:false}
            }
            
        })
        console.log(result);
        setTimeSlotModelList(result);
    },[refreshing])

    const changeAvailableSlot=(value,newValue)=>{
        let index = timeSlotModelList.indexOf(value);
        timeSlotsChange=timeSlotModelList;
        timeSlotsChange[index].selected=newValue;
        console.log(timeSlotsChange[index]);
        setTimeSlotModelList(timeSlotsChange);
        console.log(timeSlotModelList)
    }

    const rendertimeSlots=timeSlotModelList.map(function(val,index){
        // const [selected,setSelected]=useState(false);
        // useEffect(()=>{
        //     if(timeSlotList.includes(val)){setSelected(true)}
        // },[])
        return (
            <Block style={{flexDirection: 'row',}}>
                <Text size={12}>{val.time}</Text>
                <CheckBox
                    disabled={false}
                    value={val.selected}
                    onValueChange={(newValue) => changeAvailableSlot(val,newValue)}
                />
            </Block>
        )
    })
    const updateTimeSlots=async ()=>{
        let newTimeSlots=timeSlotModelList.filter(function(value){
            if(value.selected){
                return true
            }
            else{
                return false
            }
        }).map(function(value,index){return value.time});
        console.log(newTimeSlots)
      await courtDB.doc(user).update({timeSlot:newTimeSlots}).then(()=>navigation.navigate("Profile")).catch((err)=>console.log(err));
    }
    return(
        <ScrollView style={{padding:15}} refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
            {/* <Text size={16}>Edit Timeslots</Text> */}
            {rendertimeSlots}
            <Button  title="Update" onPress={()=>updateTimeSlots()}/>
        </ScrollView>
    )
}
export default EditTimeSlots;
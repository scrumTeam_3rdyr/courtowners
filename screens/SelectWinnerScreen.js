import React, { useCallback, useEffect, useState } from 'react';
import { StyleSheet, Dimensions, ScrollView, FlatList, TouchableOpacity, Modal } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import MatchItem from '../components/MatchItem';
//import { matches } from '../Data/DummyData';
import firebase from '../firebase';
import Match from '../models/match';
import Team from '../models/team';
import Player from '../models/player';
import Leaderboard from '../models/leaderboard';

const getMatches = async () => {
    const db = firebase.firestore()
    const court = firebase.auth().currentUser.email;
    const ref = db.collection("court").doc('niroshancourt@mail.com').collection('matches');
    const matchesSnapshot = await ref.get()
    const matches = matchesSnapshot.docs.map(matchSnapshot => {
        return matchSnapshot.data();
    })
    const loadedmatches = [];
    for (const key in matches) {// noway to filter dates
        loadedmatches.push(new Match(
            matches[key].id,
            matches[key].team1Id,
            matches[key].team1Name,
            matches[key].team1ImageUrl,
            matches[key].team2Id,
            matches[key].team2Name,
            matches[key].team2ImageUrl,
            matches[key].date,
            matches[key].time_slot,
            matches[key].isResultsUpdated,
            matches[key].winner, //id
        ));
    }
    return loadedmatches;
}

const getTeams = async (teamId) => {
    const db = firebase.firestore();
    const ref = db.collection("team");
    const teamSnapshot = await ref.get();
    const teams = teamSnapshot.docs.map(teamSnapshot => {
        return teamSnapshot.data();
    });
    const loadedteams = [];
    for (const key in teams) {
        loadedteams.push(new Team(
            teams[key].id,
            teams[key].teamName,
            teams[key].imageUrl,
            teams[key].currentPlayers,
            teams[key].teamCaptain,
            teams[key].loss,
            teams[key].wins
        ));
    }
    return loadedteams;

}

const getTeamReaults = async () => {
    const db = firebase.firestore()
    const court = firebase.auth().currentUser.email;
    const ref = db.collection("court").doc('niroshancourt@mail.com').collection('result');

    const resultSnapshot = await ref.get();

    const results = resultSnapshot.docs.map(resultSnapshot => {
        return resultSnapshot.data();
    })
    const loadedresults = [];
    for (const key in results) {// noway to filter dates
        loadedresults.push(new Leaderboard(
            results[key].teamId,
            results[key].teamName,
            results[key].wins,
            results[key].losses,
            results[key].matchCount,
            results[key].points
        ));
    }
    return loadedresults;




}

const getPlayers = async () => {
    const db = firebase.firestore();
    const ref = db.collection("player");
    const playerSnapshot = await ref.get();
    const players = playerSnapshot.docs.map(playerSnapshot => {
        return playerSnapshot.data();
    });
    const loadedplayers = [];
    for (const key in players) {
        loadedplayers.push(new Player(
            players[key].email,
            players[key].username,
            players[key].mobile,
            players[key].current_team,
            players[key].gender,
            players[key].status,
            players[key].wins,
            players[key].loss
        ));
    }
    return loadedplayers;

}

const updateWinner = async (id, team1Id, team1Name, team1ImageUrl, team2Id, team2Name, team2ImageUrl, date, time,isResultsUpdated, winningTeam) => {
    const db = firebase.firestore();
    const court = firebase.auth().currentUser.email;
    const ref = db.collection("court").doc('niroshancourt@mail.com').collection('matches').doc(id);
    const match = new Match(id, team1Id, team1Name, team1ImageUrl, team2Id, team2Name, team2ImageUrl, date, time,isResultsUpdated, winningTeam);
    const rawMatch = JSON.parse(JSON.stringify(match));
    await ref.set(rawMatch, { merge: true });

}

const updateTeamPlayersWins = async (email, username, mobile, current_team, gender, status, wins, loss) => {
    const db = firebase.firestore();
    const ref = db.collection("player").doc(email);
    const player = new Player(email, username, mobile, current_team, gender, status, wins, loss);
    const rawPlayer = JSON.parse(JSON.stringify(player));
    await ref.set(rawPlayer, { merge: true });
};

const updateResult = async (teamId, teamName, wins, losses, matchCount, points) => {
    const db = firebase.firestore();
    const ref = db.collection("court").doc('niroshancourt@mail.com').collection('result').doc(teamId);
    const result = new Leaderboard(teamId, teamName, wins, losses, matchCount, points);
    const rawResult = JSON.parse(JSON.stringify(result));
    await ref.set(rawResult, { merge: true });
}

const createNewResult = async (teamId, teamName, wins, losses, matchCount, points) => {
    const db = firebase.firestore();
    const court = firebase.auth().currentUser.email;
    const ref = db.collection('court').doc('niroshancourt@mail.com').collection('result').doc(teamId);
    const result = new Leaderboard(teamId, teamName, wins, losses, matchCount, points);
    const rawResult = JSON.parse(JSON.stringify(result));
    await ref.set(rawResult);
}
// const updateTeamWins = () =>{

// }

const SelectWinnerScreen = props => {

    const matchId = props.route.params.matchId;
    const teamName1 = props.route.params.teamName1;
    const teamName2 = props.route.params.teamName2;
    const teamId1 = props.route.params.teamId1;
    const teamId2 = props.route.params.teamId2;

    const [matchData, setMatchData] = useState([]);
    const [teamData, setTeamData] = useState([]);
    const [playerData, setPlayerData] = useState([]);
    const [resultData, setResultData] = useState([]);

    useEffect(() => {
        const getMatchData = async () => {
            const data = await getMatches();
            setMatchData(data);
            const tdata = await getTeams();
            setTeamData(tdata);
            const pdata = await getPlayers();
            setPlayerData(pdata);
            const rdata = await getTeamReaults();
            setResultData(rdata);
        };
        getMatchData();

    }, [value]);

    let radio_props = [
        { label: teamName1, value: teamId1 },
        { label: teamName2, value: teamId2 }
    ];

    const [value, setValue] = useState(teamId1); //team //winner

    useEffect(() => { console.log("selected", value); }, [value]);



    const markWinnerHandler = async () => {
        //console.log(value, matchData, matchId, teamId1);
        const selectdeMatch = matchData.find(match => match.id === matchId);
        // console.log(selectdeMatch);
        await updateWinner(matchId, teamId1, teamName1, selectdeMatch.team1ImageUrl, teamId2, teamName2, selectdeMatch.team2ImageUrl, selectdeMatch.date, selectdeMatch.time_slot, true, value);
        //update winner

        const selectedTeam = teamData.find(team => team.id === value);
        const defeatTeam = teamData.find(team => team.id === (value === teamId1 ? teamId2 : teamId1));

        for (let index = 0; index < 6; index++) {
            const selectedPlayer = playerData.find(player => player.email === selectedTeam.currentPlayers[index]);

            await updateTeamPlayersWins(
                selectedPlayer.email,
                selectedPlayer.username,
                selectedPlayer.mobile,
                selectedPlayer.current_team,
                selectedPlayer.gender,
                selectedPlayer.status,
                selectedPlayer.wins + 1,
                selectedPlayer.loss);

            const selecteddefeatPlayer = playerData.find(player => player.email === defeatTeam.currentPlayers[index]);
            await updateTeamPlayersWins(
                selecteddefeatPlayer.email,
                selecteddefeatPlayer.username,
                selecteddefeatPlayer.mobile,
                selecteddefeatPlayer.current_team,
                selecteddefeatPlayer.gender,
                selecteddefeatPlayer.status,
                selecteddefeatPlayer.wins,
                selecteddefeatPlayer.loss + 1);
        }

        


       
        console.log("result", resultData, value, teamId1, teamId2);

        
        //console.log(selectedWinner);
        if (resultData.length === 0) {// if  it is the first time that enter results
             console.log("result is thereeeeeee");
             await createNewResult(
                 (value === teamId1 ? teamId1 : teamId2),
                 (value === teamId1 ? teamName1 : teamName2),
                 1,
                 0,
                 1,
                 2
             );
             await createNewResult(
                (value === teamId1 ? teamId2 : teamId1),
                (value === teamId1 ? teamName2 : teamName1),
                0,
                1,
                1,
                -2
            );
        };
        const selectedWinner = resultData.find(team => team.teamId === value);
        const selectedLooser = resultData.find(team => team.teamId === (value === teamId1 ? teamId2 : teamId1));
        if(typeof selectedWinner === 'undefined'){//when winning team is new
            await createNewResult(
                (value === teamId1 ? teamId1 : teamId2),
                (value === teamId1 ? teamName1 : teamName2),
                1,
                0,
                1,
                2
            );
        }
        if(typeof selectedLooser === 'undefined'){//when looser is new

            await createNewResult(
                (value === teamId1 ? teamId2 : teamId1),
                (value === teamId1 ? teamName2 : teamName1),
                0,
                1,
                1,
                -2
            );
        }
        
        else{
            await updateResult(
                selectedWinner.teamId,
                selectedWinner.teamName,
                selectedWinner.wins + 1,
                selectedWinner.losses,
                selectedWinner.matchCount + 1,
                selectedWinner.points + 2);
          
            await updateResult(
                selectedLooser.teamId,
                selectedLooser.teamName,
                selectedLooser.wins,
                selectedLooser.losses + 1,
                selectedLooser.matchCount + 1,
                selectedLooser.points - 2);
        }
        
       


        props.navigation.navigate("AddMatchResults");

    };

    return (
        <Block style={styles.screen}>
            <Block style={styles.container}>
                <Block style={styles.headerContainer}>
                    <Text style={styles.header}> Who is the Winner ?</Text>
                </Block>
                <Block style={styles.teamsContainer}>
                    <RadioForm
                        radio_props={radio_props}
                        initial={teamId1}
                        onPress={(value) => { setValue(value) }}
                        buttonColor='green'
                        selectedButtonColor='green'
                    />

                </Block>

                <Block style={styles.saveButtonContainer}>
                    <Button color="green" round onPress={markWinnerHandler}> Save </Button>
                </Block>

            </Block>
        </Block>

    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#dedfda',
        justifyContent: 'center',
        alignItems: 'center'

    },
    container: {
        flexDirection: 'column',
        height: theme.SIZES.BASE * 20,
        width: theme.SIZES.BASE * 21.5,
        backgroundColor: '#a7f798',
        marginHorizontal: theme.SIZES.BASE * 0.8,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: theme.SIZES.BASE,
        //borderWidth:1
    },

    headerContainer: {
        marginTop: theme.SIZES.BASE * 3
    },
    header: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    teamsContainer: {
        marginTop: theme.SIZES.BASE * 2,
        marginBottom: theme.SIZES.BASE,
        flexDirection: 'column'
    },

    teamContainer: {
        marginBottom: theme.SIZES.BASE,
        flexDirection: 'row',
        //borderWidth:1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        height: theme.SIZES.BASE * 2.5,
        width: theme.SIZES.BASE * 15,
        elevation: 5,
        borderRadius: 10
    },
    saveButtonContainer: {
        marginTop: theme.SIZES.BASE,
        marginBottom: theme.SIZES.BASE,
        //marginLeft:theme.SIZES.BASE * 6,
    }

});

export default SelectWinnerScreen;
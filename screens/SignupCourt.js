import React from 'react';
import { ImageBackground, StyleSheet, StatusBar, Dimensions, Platform, KeyboardAvoidingView, TouchableOpacity,Alert } from 'react-native';
import { Block, Button, Text, theme, Input } from 'galio-framework';

const { height, width } = Dimensions.get('screen');

import materialTheme from '../constants/Theme';
import Images from '../constants/Images';

import firebase from '../firebase';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.dbRef = firebase.firestore().collection('court');

    this.state = {
      email: "",
      court_name: "",
      password: "",
      confirm_password: "",
      address: "",
      error_address: "",
      error_email: "",
      error_court_name: "",
      error_password: "",
      error_confirm_password: "",
      error: "",
      
    }
  }


  empty_email_validator() {
    if (this.state.email == "") {
      this.setState({ error_email: "Please fill Email" })
    } else {
      this.setState({ error_email: "" })
    }
  }
  empty_court_name_validator() {
    if (this.state.court_name == "") {
      this.setState({ error_court_name: "Please fill court name" })
    } else {
      this.setState({ error_court_name: "" })
    }
  }
  empty_address_validator() {
    if (this.state.address == "") {
      this.setState({ error_address: "Please fill court address" })
    } else {
      this.setState({ error_address: "" })
    }
  }
  empty_password_validator() {
    if (this.state.password == "") {
      this.setState({ error_password: "Please fill password" })
    } else {
      this.setState({ error_password: "" })
    }
  }
  empty_confirm_password_validator() {
    if (this.state.confirm_password == "") {
      this.setState({ error_confirm_password: "Please fill confirm password" })
    } else {
      this.setState({ error_confirm_password: "" })
    }
  }
  password_validator() {
    if (this.state.password != this.state.confirm_password) {
      this.setState({ error: "passwords does not match" })
    } else {
      this.setState({ error: "" })
    }
  }

  updateInputVal = (val, prop) => {
    console.log("update individual " + val);
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  handleSignUp = () => {
    const { court_name, password, email, address } = this.state
    firebase.auth()
      .createUserWithEmailAndPassword(email, password)
      .catch(error => Alert.alert(error.code))
    const ref = this.dbRef.doc(email);
    ref.set({
      email: email,
      court_name: court_name,
      address: address,
      active: false
    })
     .then(() => this.props.navigation.navigate('App'))
      .catch(error => Alert.alert(error.code))
  }


  render() {
    const { navigation } = this.props;

    return (
      <Block style={styles.container}>
        <KeyboardAvoidingView behavior="height" enabled style={{ flex: 1 }}>
          <StatusBar barStyle="light-content" />
          <Block row flex space="around" style={{ zIndex: 1 }}>
            <Block flex center>
              <Block>
                <Text color="black" size={60}>SignUp</Text>
              </Block>
              <Block>
                {/* <Input
                  placeholder="Mobile Number"
                  minLenght={10}
                  maxLength={10}
                  placeholderTextColor="black"
                  type="number-pad"
                  color="black"
                  style={styles.input}
                  value={this.state.mobile_number}
                  onChangeText={(val) => this.updateInputVal(val, 'mobile_number')}
                  //onChange={(Value) => this.setState({ mobile_number: Value })}
                  onBlur={() => this.empty_mobile_number_validator()}
                /> */}
                <Input
                  placeholder="Email"
                  placeholderTextColor="black"
                  email
                  color="black"
                  style={styles.input}
                  value={this.state.email}
                  onChangeText={(val) => this.updateInputVal(val, 'email')}
                  //onChange={(Value) => this.setState({ mobile_number: Value })}
                  onBlur={() => this.empty_email_validator()}
                  autoCapitalize='none'
                />
                <Text style={{ color: 'red', marginLeft: 20 }}>
                  {this.state.error_email}
                </Text>
                <Input
                  placeholder="Court Name"
                  minLenght={3}
                  placeholderTextColor="black"
                  color="black"
                  style={styles.input}
                  value={this.state.court_name}
                  onChangeText={(val) => this.updateInputVal(val, 'court_name')}
                 // onChange={(Value) => this.setState({ username: Value })}
                  onBlur={() => this.empty_court_name_validator()}
                />
                <Text style={{ color: 'red', marginLeft: 20 }}>
                  {this.state.error_court_name}
                </Text>
                <Input
                  placeholder="Address"
                  minLenght={3}
                  placeholderTextColor="black"
                  color="black"
                  style={styles.input}
                  value={this.state.address}
                  onChangeText={(val) => this.updateInputVal(val, 'address')}
                  //onChange={(Value) => this.setState({ address: Value })}
                  onBlur={() => this.empty_address_validator()}
                />
                <Text style={{ color: 'red', marginLeft: 20 }}>
                  {this.state.error_address}
                </Text>
                <Input
                  placeholder="Password"
                  minLenght={6}
                  password
                  viewPass
                  placeholderTextColor="black"
                  color="black"
                  iconColor="black"
                  style={styles.input}
                  value={this.state.password}
                  onChangeText={(val) => this.updateInputVal(val, 'password')}
                  //onChange={(Value) => this.setState({ password: Value })}
                  onBlur={() => this.empty_password_validator()}
                />
                <Text style={{ color: 'red', marginLeft: 20 }}>
                  {this.state.error_password}
                </Text>
                <Input
                  placeholder="Confirm Password"
                  minLenght={6}
                  password
                  viewPass
                  placeholderTextColor="black"
                  color="black"
                  iconColor="black"
                  style={styles.input}
                  value={this.state.confirm_password}
                  onChangeText={(val) => this.updateInputVal(val, 'confirm_password')}
                  //onChange={(Value) => this.setState({ confirm_password: Value })}
                  //onBlur={() => this.empty_confirm_password_validator()}
                  onBlur={() => this.password_validator()}
                />
                {/* <Text style={{ color: 'red', marginLeft: 20 }}>
                  {this.state.error_confirm_password}
                </Text> */}
                <Text style={{ color: 'red', marginLeft: 20 }}>
                  {this.state.error}
                </Text>
              </Block>

              <Button
                shadowless
                style={styles.button}
                color={materialTheme.COLORS.GREEN}
                //onPress={() => navigation.navigate('App')}
                onPress={() => this.handleSignUp()}>
                  
                SignUp
              </Button>
              <Block row>
                <Text color="black"> Do you already have an account?</Text>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Login')}>
                  <Text color={materialTheme.COLORS.GREEN} style={{ fontWeight: "bold" }}>Login</Text>
                </TouchableOpacity>
              </Block>

            </Block>
          </Block>
        </KeyboardAvoidingView>

      </Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  padded: {
    paddingHorizontal: theme.SIZES.BASE * 1,
    position: 'relative',
    bottom: theme.SIZES.BASE * 3,
  },
  button: {
    width: width - theme.SIZES.BASE * 10,
    height: theme.SIZES.BASE * 3,
    shadowRadius: 0,
    shadowOpacity: 0,
  },
  input: {
    width: width - theme.SIZES.BASE * 4,
    height: theme.SIZES.BASE * 3,

  },
  input: {
    width: width - theme.SIZES.BASE * 4,
    height: theme.SIZES.BASE * 3,
    borderColor: "white",
    borderBottomColor: "black",
    backgroundColor: "white"
  },
});


import React from 'react';
import { StyleSheet, Dimensions, ScrollView ,Image,View} from 'react-native';
import { Button, Block, Text, theme } from 'galio-framework';

import { Icon, Product } from '../components/';

const { width,height  } = Dimensions.get('screen');
import products from '../constants/products';
import {  PricingCard,Card  } from 'react-native-elements';


export default class Home extends React.Component {
 

  render() {
    const { navigation } = this.props;
    return (
      
      <Block style={styles.home}>
      

      <View >
          
          <Image source = {require('../assets/images/image.jpg')}style={styles.imageViewHead}   />
          {/* <Text style={styles.headText}>Welcome</Text> */}
        </View>

      
     
    <ScrollView>
      {/* <PricingCard
        color="#0CBD16"
        price="Bookings"
        image={{ uri: '../assets/images/free-soccer-field-vector.jpg'}}
        button={{ title: 'View'}}
        onButtonPress={()=>navigation.navigate('Reservation')}
      /> */}

      <PricingCard
        color="#0CBD16"
        price="Leaderboard"
        button={{ title: 'Update '}}
        onButtonPress={()=>navigation.navigate('AddMatchResults')}
      />


  </ScrollView>
  
  </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  
  
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  banner: { justifyContent: "center", alignItems: "center" },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
  },
  imageView: {
    width: width/5,
    height: width/5 ,
    margin: 7,
    borderRadius : 7,
},
head:{
  width:width,
  height:width/2.5,
  backgroundColor:"#3BAD36",
  justifyContent:"center",
},
headText:{
  alignSelf:"center",
  fontSize:20,
  color:"white",
  
},
headText:{
  padding:10,

  alignSelf:"center",
  fontSize:30,
  color:"black",
  
},

imageViewHead:{
  width: width/1,
    height: height/3.5 ,
    margin: 7,
    borderRadius : 7,
    alignSelf:"center"
}

});


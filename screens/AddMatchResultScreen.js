import React, { useState, useEffect } from 'react';
import { StyleSheet, Dimensions, ScrollView, FlatList, TouchableOpacity, Modal } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';
import { Icon, Product } from '../components/';


import MatchItem from '../components/MatchItem';
import { matches } from '../Data/DummyData';
import firebase from '../firebase';
import Match from '../models/match';
import Team from '../models/team';

const { width, height } = Dimensions.get('screen');

const getMatches = async () => {
    const db = firebase.firestore()
    const court = firebase.auth().currentUser.email;
    const ref = db.collection("court").doc('niroshancourt@mail.com').collection('matches');
    const matchesSnapshot = await ref.get();
    const matches = matchesSnapshot.docs.map(matchSnapshot => {
        return matchSnapshot.data();
    })
    const loadedmatches = [];
    for (const key in matches) {// noway to filter dates
        if (!matches[key].isResultsUpdated) {
            loadedmatches.push(new Match(
                matches[key].id,
                matches[key].team1Id,
                matches[key].team1Name,
                matches[key].team1ImageUrl,
                matches[key].team2Id,
                matches[key].team2Name,
                matches[key].team2ImageUrl,
                matches[key].date,
                matches[key].time_slot,
                matches[key].isResultsUpdated,
                matches[key].winner, //id
            ));
        }
    }
    return loadedmatches;
}

const AddMatchResultsScreen = props => {

    const [matchData, setMatchData] = useState([]);

    useEffect(() => {
        const getMatchData = async () => {
            const data = await getMatches();
            setMatchData(data);
        };
        getMatchData();
    }, []);


    const renderMatches = itemData => {
        //console.log("match data", matchData);
        return (
            <MatchItem
                team1Name={itemData.item.team1Name}
                logo1={itemData.item.team1ImageUrl}
                team2Name={itemData.item.team2Name}
                logo2={itemData.item.team2ImageUrl}
                date={itemData.item.date}
                time={itemData.item.time}
                onSelect={() => {
                    props.navigation.navigate('SelectWinner', {
                        matchId: itemData.item.id,
                        teamId1: itemData.item.team1Id,
                        teamId2: itemData.item.team2Id,
                        teamName1: itemData.item.team1Name,
                        teamName2: itemData.item.team2Name
                    });
                }}
            />
        );
    };
    return (
        <Block style={styles.screen}>
            <FlatList
                data={matchData}
                renderItem={renderMatches}
            />
        </Block>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#dedfda'

    },

});

export default AddMatchResultsScreen;
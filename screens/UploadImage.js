import React, { useState,useEffect } from 'react';
import {
  View,
  SafeAreaView,
  Text,
  TouchableOpacity,
  StyleSheet,
  Platform,
  Alert,
  Button,
  Image
} from 'react-native';
//import ImagePicker from 'react-native-image-picker';
import firebase from '../firebase';
import * as ImagePicker from 'expo-image-picker';

export default function Upload(props){
    const {navigation} = props;
    const [image, setImage] = useState(null);
    const [uploading, setUploading] = useState(false);
    const [transferred, setTransferred] = useState(0);
    const storageRef=firebase.storage();
    const fireStoreRef = firebase.firestore().collection('court').doc(firebase.auth().currentUser.email);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);
  const uploadImage = async () => {
            const { uri } = image;
            const filename = firebase.auth().currentUser.email;
            const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
            setUploading(true);
            setTransferred(0);
            const response = await fetch(image.uri);
            const file= await response.blob();
            const task = storageRef.ref(filename).put(file);
            task.on(
                "state_changed",
                snapshot=>{},
                err=>console.log(err),
                async ()=>{
                    const uri = await task.snapshot.ref.getDownloadURL();
                    console.log(uri)
                    fireStoreRef.update({imageuri:uri}).then(()=>{
                        console.log("written");
                        navigation.navigate('EditProfile')
                    }).catch(err=>console.log(err))
                }
                
            )
            // set progress state
            task.on('state_changed', snapshot => {
              setTransferred(
                Math.round(snapshot.bytesTransferred / snapshot.totalBytes) * 10000
              );
            });
            try {
              await task;
            } catch (e) {
              console.error(e);
            }
            setUploading(false);
            Alert.alert(
              'Photo uploaded!',
              'Your photo has been uploaded to Firebase Cloud Storage!'
            );

            setImage(null);
          };
  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled && result.type==='image') {
      setImage({uri:result.uri});
    }
    console.log(image.uri)
  };

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button title="Pick an image from camera roll" onPress={pickImage} />
      {image && <Image source={image} style={{ width: 200, height: 200 }} />}

      <Button title="upload" onPress={uploadImage} />
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#bbded6'
    },
    selectButton: {
      borderRadius: 5,
      width: 150,
      height: 50,
      backgroundColor: '#8ac6d1',
      alignItems: 'center',
      justifyContent: 'center'
    },
    uploadButton: {
      borderRadius: 5,
      width: 150,
      height: 50,
      backgroundColor: '#ffb6b9',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 20
    },
    buttonText: {
      color: 'white',
      fontSize: 18,
      fontWeight: 'bold'
    },
    imageContainer: {
      marginTop: 30,
      marginBottom: 50,
      alignItems: 'center'
    },
    progressBarContainer: {
      marginTop: 20
    },
    imageBox: {
      width: 300,
      height: 300
    }
});
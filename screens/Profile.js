import React,{useState, useEffect} from 'react';
import { 
  StyleSheet, 
  Dimensions, 
  ScrollView, 
  Image, 
  RefreshControl,
  ImageBackground, 
  Platform,
  Button 
} from 'react-native';
import { Block, Text, theme, } from 'galio-framework';
import { LinearGradient } from 'expo-linear-gradient';
import {Avatar,Header,Icon} from 'react-native-elements';
import { Images, materialTheme } from '../constants';
import { HeaderHeight } from "../constants/utils";
import firebase from "../firebase";

const { width, height } = Dimensions.get('screen');
const thumbMeasure = (width - 48 - 32) / 3;

const wait = (timeout) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}

function Profile (props) {
  const {navigation}=props
  const courtDB=firebase.firestore().collection('court');
  const [address, setAddress] = useState('');
  const [Name, setName] = useState('');
  const [ContactNo, setContactNo] = useState('');
  const [ProfilePicture, setProfilePicture] = useState(null);
  const [CourtImages,setCourtImages]=useState([]);
  const [timeSlots, setTimeSlots] = useState([]);
  //refreshing
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(()=>{
    var query=courtDB.doc(firebase.auth().currentUser.email).get().then(
      (doc)=>{
          let data = doc.data();
          setAddress(data.address);
          setName(data.court_name);
          if(data.contact_no){setContactNo(data.contact_no)}
          if(data.imageuri){setProfilePicture(data.imageuri);}
          if(data.court_images){setCourtImages(data.court_images)}
          if(data.timeSlot){setTimeSlots(data.timeSlot)}
        }
    );
  },[refreshing]) 

  const renderTimeslots=timeSlots.map(function(val,index){
    return <Text size={16}>{val}</Text>
    })
    return (
      <Block flex >
        <ScrollView style={styles.ScrollViewStyle} refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>

        <Block flex style={{alignItems: 'center',}}>
          {ProfilePicture!==null? (
            <Avatar
            rounded
            size="xlarge"
            source={{ uri:ProfilePicture}}
            overlayContainerStyle={{backgroundColor: 'grey'}}
          />
          ):(
            <Avatar
            rounded
            size="xlarge"
            icon={{name: 'user', type: 'font-awesome'}}
            overlayContainerStyle={{backgroundColor: 'grey'}}
          />
          )}
        

         <Text size={20}>{Name}</Text>
        </Block>
        <Block style={{padding: 20,}}>
          <Text size={16}>Address : {address}</Text>
          <Text size={16}>Contact Number:{ContactNo}</Text>
        </Block>
        <Block style={{padding: 20,}}>
          <Button onPress={()=>navigation.navigate("EditProfile")} 
          title="Edit Profile" color={materialTheme.COLORS.GREEN}/>
        </Block>
        <Block style={{padding: 20,}}>
        <Block row space="between" style={{ paddingVertical: 16, alignItems: 'baseline' }}>
              <Text size={20}>Time Slots</Text>
              <Text size={12} color={theme.COLORS.PRIMARY} 
                onPress={() => navigation.navigate('EditTimeSlots')}>Edit Timeslots</Text>
            </Block>
          {renderTimeslots}
        </Block>
        <Block flex style={styles.gallery}>
           <ScrollView showsVerticalScrollIndicator={true}> 
           
            <Block row space="between" style={{ paddingVertical: 16, alignItems: 'baseline' }}>
              <Text size={20}>Court Images</Text>
              <Text size={12} color={theme.COLORS.PRIMARY} 
                onPress={() => navigation.navigate('UploadCourtImages')}>Add</Text>
            </Block>
            <Block style={{ paddingBottom: -HeaderHeight * 2 }}>
              <Block row space="between" style={{ flexWrap: 'wrap' }} >
                {CourtImages.map((img, imgIndex) => (
                  <Image
                    source={{ uri: img }}
                    key={`viewed-${img}`}  
                    resizeMode="cover"
                    style={styles.thumb}
                  />
                ))}
              </Block>
            </Block>
          </ScrollView>
        </Block>
        </ScrollView>
      </Block>
    );
  
}

export default Profile;

const styles = StyleSheet.create({
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    marginBottom: -HeaderHeight * 2,
  },
  profileImage: {
    width: width * 1.1,
    height: 'auto',
  },
  profileContainer: {
    width: width,
    height: height / 2,
  },
  profileDetails: {
    paddingTop: theme.SIZES.BASE * 4,
    justifyContent: 'flex-end',
    position: 'relative',
  },
  profileTexts: {
    paddingHorizontal: theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
    zIndex: 2
  },

  seller: {
    marginRight: theme.SIZES.BASE / 2,
  },
  options: {
    position: 'relative',
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: -theme.SIZES.BASE * 7,
    borderTopLeftRadius: 13,
    borderTopRightRadius: 13,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure
  },
  gradient: {
    zIndex: 1,
    left: 0,
    right: 0,
    bottom: 0,
    height: '30%',
    position: 'absolute',
  },
  ScrollViewStyle:{
    paddingTop:width*0.05
  },
  gallery:{
    paddingHorizontal:width*0.05
  }
});

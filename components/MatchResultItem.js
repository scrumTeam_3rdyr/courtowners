import React from 'react';
import { TouchableWithoutFeedback, StyleSheet, ImageBackground, Dimensions, Image } from 'react-native';

import { Block, Text, theme, Button, Input } from 'galio-framework';
import { Images, materialTheme } from '../constants';


const { width, height } = Dimensions.get('screen');

const MatchResultItem = props => {

    
    return (
        <Block style={styles.mealItem}>
            <TouchableWithoutFeedback
                onPress={() => { }}
                // onPress={props.onSelect}
                style={{ flex: 1 }}>
                <Block style={styles.container}>
                
                    <Block style={styles.teamContainer}>
                        <Block style={styles.imageContainer}>
                            <Image source={{ uri: (props.winner === props.team1Id ? props.logo1 : props.logo2) }} style={styles.image} />
                        </Block>
                        <Block style={styles.teamNameContainer}>
                            <Text style={styles.teamName}>{props.winner === props.team1Id ? props.team1Name:props.team2Name}</Text>

                        </Block>
                        <Block style={styles.result}>
                            {/* <Text style ={styles.winner}>W</Text> */}
                            <Text style={styles.winner}>W</Text>
                        </Block>

                    </Block>
                    <Block style={styles.dateTimeContainer}>
                        <Block style={styles.dateContainer}>
                            <Text style={styles.date}>{props.date}</Text>
                        </Block>
                        <Block style={styles.timeContainer}>
                            <Text style={styles.time}>{props.time}</Text>
                        </Block>

                        {/* <Block style={styles.imageContainer}>
                            <Image source={{ uri: props.logo1 }} style={styles.image} />
                        </Block>
                        <Block style={styles.teamName}>
                            <Text>{props.team1Name}</Text>

                        </Block> */}
                    </Block>
                    <Block style={styles.teamContainer}>
                        <Block style={styles.imageContainer}>
                            <Image source={{ uri: (props.winner != props.team1Id ? props.logo1 : props.logo2) }} style={styles.image} />
                        </Block>
                        <Block style={styles.teamNameContainer}>
                            <Text style={styles.teamName}>{props.winner != props.team1Id ? props.team1Name : props.team2Name}</Text>

                        </Block >
                        <Block style={styles.result}>
                            {/* <Text style={styles.loser}>L</Text> */}
                            <Text style={styles.loser}>L</Text>
                        </Block>

                    </Block>

                    {/* <Block style={styles.buttonContainer}>
                        <Button
                            shadowless
                            style={styles.button}
                            color={materialTheme.COLORS.SUCCESS}
                            // color='lightgreen'
                            onPress={props.onRequest}

                        >Request</Button>


                    </Block> */}

                </Block>

            </TouchableWithoutFeedback>
        </Block>


    );
};

const styles = StyleSheet.create({
    mealItem: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 4,
        overflow: 'hidden',


    },
    container: {
        flexDirection: 'row',
        padding: 10,
        paddingHorizontal: 10,
        // justifyContent: 'space-between',
        alignItems: 'center',
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        elevation: 1,
        borderRadius: 10,
        height: theme.SIZES.BASE * 8,
        width: theme.SIZES.BASE * 23.5,


        //     flex: 1,
        //     borderRadius: 10,
        //    // borderWidth: 3,
        //     height: 200,
        //     width: 350,
        //     //backgroundColor: '#ffffd5',
        //     elevation:1

    },
    teamContainer: {
        flexDirection: 'column',
        //justifyContent:'center',
        alignItems: 'center',
        // borderWidth: 1,
        height: theme.SIZES.BASE * 7,
        width: theme.SIZES.BASE * 8.3,
    },
    teamName: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#383b3c'
    },
    dateTimeContainer: {
        flexDirection: 'column',
        //justifyContent:'center',
        alignItems: 'center',
        // borderWidth: 1,
        height: theme.SIZES.BASE * 7,
        width: theme.SIZES.BASE * 5.5,
    },
    result: {
        height: theme.SIZES.BASE
    },
    winner: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'green'
    },
    loser: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'red'
    },
    teamNameContainer: {
        // paddingHorizontal: 10,
        paddingVertical: 7,
        height: theme.SIZES.BASE * 2.5,
        // margin: 20
    },
    imageContainer: {
        marginRight: theme.SIZES.BASE * 2.5,
        marginLeft: theme.SIZES.BASE * 2.5,
        // borderWidth: 1,
        height: theme.SIZES.BASE * 3,
        width: theme.SIZES.BASE * 3,
        overflow: 'hidden',
    },
    dateContainer: {
        marginTop: theme.SIZES.BASE * 2,

    },
    date: {
        fontSize: 13,
        fontWeight: 'bold',
        color: '#404141'
    },
    timeContainer: {
        marginTop: theme.SIZES.BASE * 0.5,
        alignItems: 'center',
        // borderWidth:1,
        paddingHorizontal: theme.SIZES.BASE * 0.5,

    },
    time: {
        fontSize: 12,
        fontWeight: 'bold',
        color: 'gray'
    },
    image: {
        height: '100%',
        width: '100%'
    },
    // button:{
    //     width: width - theme.SIZES.BASE * 18,
    //     height: theme.SIZES.BASE * 2,
    //     shadowRadius: 0,
    //     shadowOpacity: 0,
    //     borderRadius: 50
    // },
    // buttonContainer:{
    //     margin: theme.SIZES.BASE 
    // }
});
export default MatchResultItem;
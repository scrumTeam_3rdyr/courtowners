import React from 'react';
import { TouchableWithoutFeedback, StyleSheet, ImageBackground, Dimensions, Image } from 'react-native';

import { Block, Text, theme, Button, Input } from 'galio-framework';
//import { Images, materialTheme } from '../constants';

const { width, height } = Dimensions.get('screen');

const LeaderboardItem = props => {
    return (
        <Block style ={styles.container}>
            <Block style={styles.indexContainer}>
                <Text style={styles.index}>{props.index}</Text>
            </Block>
            <Block style={styles.teamNameContainer}>
                <Text style={styles.teamName}>{props.teamName}</Text>
            </Block>
            <Block style={styles.winsContainer}>
                <Text style={styles.wins}>{props.wins}</Text>
            </Block>
            <Block style={styles.lossesContainer}>
                <Text style={styles.losses}>{props.losses}</Text>
            </Block>
            <Block style={styles.matchCountContainer}>
                <Text style={styles.matchCount}>{props.matchCount}</Text>
            </Block>
            <Block style={styles.pointsContainer}>
                <Text style={styles.points}>{props.points}</Text>
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        height: theme.SIZES.BASE *2,
        width: theme.SIZES.BASE * 23,
        backgroundColor: 'white',
        marginHorizontal: theme.SIZES.BASE *0.8  ,
        //borderWidth:1
    },
    indexContainer: {
       width: theme.SIZES.BASE *2.5,
       //borderWidth:1,
       justifyContent:'center',
        alignItems:'center'
    },
    index: {
        fontSize:14,
        fontWeight:'bold',
        color:'#737372'
    },
    teamNameContainer: {
        width: theme.SIZES.BASE *8.5,
        justifyContent:'center',
      // borderWidth:1
    },
    teamName: {
        fontSize:14,
        fontWeight:'bold',
        color:'#737372'
    },
    winsContainer: {
        width: theme.SIZES.BASE *3,
        //borderWidth:1,
        justifyContent:'center',
        alignItems:'center'
    },
    wins: {
        fontSize:14,
        fontWeight:'bold',
        color:'#737372'
    },
    lossesContainer: {
        width: theme.SIZES.BASE *3,
       // borderWidth:1,
        justifyContent:'center',
        alignItems:'center'
    },
    losses: {
        fontSize:14,
        fontWeight:'bold',
        color:'#737372'
    },
    matchCountContainer: {
        width: theme.SIZES.BASE *3,
       // borderWidth:1,
        justifyContent:'center',
        alignItems:'center'
    },
    matchCount: {
        fontSize:14,
        fontWeight:'bold',
        color:'#737372'
    },
    pointsContainer: {
        width: theme.SIZES.BASE *3,
       // borderWidth:1,
        justifyContent:'center',
        alignItems:'center'
    },
    points: {
        fontSize:14,
        fontWeight:'bold',
        color:'#737372'
    },
});

export default LeaderboardItem;